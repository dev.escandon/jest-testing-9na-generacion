const { sumarNumeros } = require('../suma')

test('La suma de los números es correcta', () => {
  expect(sumarNumeros(4, 5)).toBe(9)
})

test('La suma de los números no es correcta', () => {
  expect(sumarNumeros(8, 5)).toBe(9)
})
