const objKoder = {
  name: 'Carlos e',
  apellido: 'Hernández',
  edad: 5,
}

test('Testeando la edad de un objeto', () => {
  expect(objKoder.edad).toBeGreaterThan(45)
})

test('SnapShot de un objeto', () => {
  expect(objKoder).toMatchSnapshot()
})
