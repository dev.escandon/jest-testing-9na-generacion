test('La letra esta incluido', () => {
  expect('equipo').not.toMatch(/e/)
})

test('La letra no esta incluido', () => {
  expect('equipo').not.toMatch(/a/)
})

test('Testeando ancho de un string', () => {
  expect('hormiga').toHaveLength(4)
})

test('Testeando ancho de un string P2', () => {
  expect('hormiga').toHaveLength(7)
})

test('Testeando ancho de un array', () => {
  expect([1, 2, 3, 4, 5]).toHaveLength(4)
})

test('Testeando ancho de un array P2', () => {
  expect([1, 2, 3, 4, 5]).toHaveLength(5)
})
