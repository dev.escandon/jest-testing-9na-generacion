const sumarNumeros = (numUno, numDos) => {
  return numUno + numDos
}

const verificarSuma = (numUno, numDos, numEsperado) => {
  const suma = sumarNumeros(numUno, numDos)

  if (suma === numEsperado) {
    console.log(`La prueba de los números ${numUno} y ${numDos} es exitosa`)
  } else {
    throw new Error(
      `La prueba de los números ${numUno} y ${numDos} no fue exitosa`
    )
  }
}

verificarSuma(4, 5, 10)
